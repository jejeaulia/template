<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('table.table');
});
Route::get('/test/{angka}', function ($angka) {
    return view('test', ["angka" => $angka]);
});

Route::get('/test/{nama}', function ($nama) {
    return "halo $nama";
});

Route::get('/form', 'RegisterController@form');
Route::get('/form', 'RegisterController@sapa');
Route::get('/form', 'RegisterController@sapa_post');

Route ::get('/master', function(){
return view('master');
});

